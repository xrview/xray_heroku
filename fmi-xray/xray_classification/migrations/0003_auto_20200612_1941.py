# Generated by Django 3.0.6 on 2020-06-12 19:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('xray_classification', '0002_delete_xray'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='first_name',
            new_name='email',
        ),
        migrations.RenameField(
            model_name='user',
            old_name='last_name',
            new_name='username',
        ),
    ]
