from django import forms
from django.contrib.auth.models import User
# from xray_classification.models import User

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    # is_doctor = forms.DecimalField(max_digits=1, decimal_places=2)
    # status = forms.DecimalField(max_digits=1, decimal_places=2)
    class Meta():
        model = User
        fields = ('username', 'password', 'email')