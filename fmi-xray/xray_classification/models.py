from django.db import models

class User(models.Model):
    username = models.CharField(max_length=30)
    email = models.CharField(max_length=30)
    is_doctor = models.BooleanField(default=True)

class Roles(models.Model):
    role_name = models.CharField(max_length=30)
    doctor = models.BooleanField(default=True)

class Appointment(models.Model):
    creation_date = models.DateTimeField
    appointed_date = models.DateTimeField