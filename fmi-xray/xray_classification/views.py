from xray_classification.scripts.scripts import *
from xray_classification.scripts.vuforia_official import request_instance
from xray_classification.settings import MODEL_CL as model
from xray_classification.settings import img_size_cl as img_size

from django.http import JsonResponse
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from xray_classification.forms import UserForm
from django.contrib.auth.models import User

import cv2

import qrcode
import uuid

@csrf_exempt
def register(request):
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            
            return HttpResponse("Done")

        else:
            print(user_form.errors)

            return HttpResponse("Register failed")

@csrf_exempt
def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                email = User.objects.get(username=username).email
                response = "Login Done|" + str(username) + "|" + str(email)

                return HttpResponse(response)
            else:
                return HttpResponse("Your account was inactive.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(username, password))
            return HttpResponse("Invalid login details given")

@csrf_exempt
def upload(request):
    data = {"success": False}

    if request.method == 'POST':
        xray = request.FILES.get('xray', False)

        if xray:
            image_request = request.FILES["xray"]
            image_bytes = image_request.read()

            classify_result = get_prediction(model, image_bytes, img_size)

            if classify_result:
                data["success"] = True
                data["confidence"] = {}
                for idx, class_name in enumerate(classify_result["class_names"]):
                    data["confidence"][class_name] = classify_result["scores"][idx]
                # data["heatmap_path"] = classify_result["heatmap_url"]

            qr = qrcode.QRCode(
                version=1,
                error_correction=qrcode.constants.ERROR_CORRECT_L,
                box_size=10,
                border=4,
            )
            qrcode_path = os.path.join(settings.MEDIA_URL, 'qrcodes')
            os.makedirs(qrcode_path, exist_ok=True)
            unique_id = str(uuid.uuid4().hex)
            filename = unique_id + '.jpg'
            qrcode_path = os.path.join(qrcode_path, filename)

            qrcode_path_relative = '/' + '/'.join(qrcode_path.split('/')[1:])
            # data["qrcode_path"] = 'https://fmi-xray.herokuapp.com' + qrcode_path_relative

            qr.add_data(data)
            qr.make(fit=True)

            img = qr.make_image(fill_color="black", back_color="white")
            img.save(qrcode_path)

            access_key = "d33b8764d052c24a32e9aaaeac26edef66009951"
            secret_key = "697f643febc85070988819583fdfa5d9e76a0818"

            img = cv2.imread(qrcode_path)
            img_encoded = base64.b64encode(cv2.imencode('.jpg', img)[1]).decode()

            metadata = 'Focuseaza pe QR CODE'
            data_bytes = str(metadata).encode('ascii')
            base64_bytes = base64.b64encode(data_bytes)
            base64_data = base64_bytes.decode('ascii')

            status, body = request_instance(access_key=access_key,
                                            secret_key=secret_key,
                                            target_uuid=unique_id,
                                            img_encoded=img_encoded,
                                            metadata=base64_data)

            if status == 200:
                print(body)
            else:
                print(status)
                print(body)

            return render(request, 'upload.html', {'qrcode_path': qrcode_path_relative})

    return render(request, 'upload.html')

@csrf_exempt
def classify_api(request):
    data = {"success": False}

    if request.method == "POST":
        if request.FILES.get("image", None) is not None:
            image_request = request.FILES["image"]
            image_bytes = image_request.read()

        classify_result = get_prediction(model, image_bytes, img_size)

        if classify_result:
            data["success"] = True
            data["confidence"] = {}
            for idx, class_name in enumerate(classify_result["class_names"]):
                data["confidence"][class_name] = classify_result["scores"][idx]
            data["heatmap_path"] = classify_result["heatmap_url"]

    return JsonResponse(data)

def index(request):

    return render(request, 'base.html')

