"""xray_classification URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.static import serve

from . import views, settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name='home'),
    path('upload/', views.upload, name='upload'),
    path('predict/', views.classify_api, name='predict'),
    path('register/', views.register, name='register'),
    path('login/', views.user_login, name='login'),
    path('admin/', admin.site.urls),
    re_path(r'^media/(?P<path>.*)$', serve, kwargs=dict(document_root=settings.MEDIA_ROOT)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
