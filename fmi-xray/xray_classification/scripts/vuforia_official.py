from http.client import HTTPSConnection
import hashlib
import hmac
import base64
from email.utils import formatdate
import json
import sys

from wsgiref.handlers import format_date_time
from datetime import datetime
from time import mktime

# The hostname of the Vuforia Web Services API
VWS_HOSTNAME='vws.vuforia.com'


def compute_md5_hex(data):
    """Return the hex MD5 of the data"""
    h = hashlib.md5()
    h.update(data.encode('utf-8'))
    return h.hexdigest()


def compute_hmac_base64(key, data):
    """Return the Base64 encoded HMAC-SHA1 using the provide key"""
    h = hmac.new(key.encode('utf-8'), data.encode('utf-8'), hashlib.sha1)

    return base64.b64encode(h.digest())


def authorization_header_for_request(access_key, secret_key, method, content, content_type, date, request_path):
    """Return the value of the Authorization header for the request parameters"""
    components_to_sign = list()
    components_to_sign.append(method)
    components_to_sign.append(str(compute_md5_hex(content)))
    components_to_sign.append(str(content_type))
    components_to_sign.append(str(date))
    components_to_sign.append(str(request_path))
    string_to_sign = "\n".join(components_to_sign)
    signature = compute_hmac_base64(secret_key, string_to_sign)
    auth_header = "VWS %s:%s" % (access_key, signature.decode("ascii"))
    return auth_header


def request_instance(access_key, secret_key, img_encoded, target_uuid, metadata):
    """Make an HTTPS request to create a VuMark instance and return the HTTP status code and body"""
    http_method = 'POST'

    # date = formatdate(None, localtime=False, usegmt=True)
    now = datetime.now()
    stamp = mktime(now.timetuple())
    date = format_date_time(stamp)

    content_type = 'application/json'
    path = "/targets"

    # The body of the request is JSON and contains one attribute, the instance ID of the VuMark
    content = {
        "name": str(target_uuid),
        "width": 0.06,
        "image": img_encoded,
        "application_metadata": metadata,
    }
    request_body = json.dumps(content)

    # Sign the request and get the Authorization header
    auth_header = authorization_header_for_request(access_key, secret_key, http_method, request_body, content_type,
                                                   date, path)
    request_headers = {
        'Accept': 'application/json',
        'Authorization': auth_header,
        'Content-Type': content_type,
        'Date': date
    }

    # Make the request over HTTPS on port 443
    http = HTTPSConnection(VWS_HOSTNAME, 443)
    http.request(http_method, path, request_body, request_headers)

    response = http.getresponse()
    response_body = response.read()

    return response.status, response_body